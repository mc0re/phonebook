using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhonebookLib;
using SQLDatabase.Net.SQLDatabaseClient;


namespace PhonebookLibTest
{
    //[TestClass]
    public class SqlPhonebookShould
    {
        #region Constants


        #endregion


        #region Tests

        //[TestMethod]
        public async Task Sql_AddAndList()
        {
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, SqlPhonebook>()
                .AddSingleton(new SqlSettings
                {
                    ConnectionType = typeof(SqlDatabaseConnection),
                    ConnectionString = "uri=@memory"
                });
            var sp = services.BuildServiceProvider();

            var sut = sp.GetService<IPhonebook>();

            await sut.Add(
                new PhonebookEntry { Id = 1, FirstName = "fname1", LastName = "lname1", BirthYear = 2000 });
            var list = await sut.GetList();

            Assert.AreEqual(1, list.Count);

            var item1 = list[0];
            Assert.AreEqual(1, item1.Id);
            Assert.AreEqual("fname1", item1.FirstName);
            Assert.AreEqual("lname1", item1.LastName);
            Assert.AreEqual(2000, item1.BirthYear);
        }

        #endregion
    }
}
