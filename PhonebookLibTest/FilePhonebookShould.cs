using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhonebookLib;


namespace PhonebookLibTest
{
    [TestClass]
    public class FilePhonebookShould
    {
        #region Constants

        private const string SamplePhonebook = @"<?xml version=""1.0""?>
<ArrayOfPhonebookEntry xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <PhonebookEntry>
    <Id>1</Id>
    <FirstName>First</FirstName>
    <LastName>Last</LastName>
    <BirthYear>1800</BirthYear>
  </PhonebookEntry>
  <PhonebookEntry>
    <Id>2</Id>
    <FirstName>Second</FirstName>
    <LastName>Last</LastName>
    <BirthYear>1900</BirthYear>
  </PhonebookEntry>
</ArrayOfPhonebookEntry>";

        #endregion


        #region Tests

        [TestMethod]
        public async Task File_CreateOnListWhenAbsent()
        {
            var pbFile = Path.Combine(Path.GetTempPath(), "pb.xml");
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, MockFileSystem>()
                .AddSingleton(new FileSettings { Path = pbFile });
            var sp = services.BuildServiceProvider();

            var sut = sp.GetService<IPhonebook>();

            var list = await sut.GetList();

            Assert.AreEqual(0, list.Count);
            Assert.IsTrue(sp.GetService<IFileSystem>().File.Exists(pbFile));
        }


        [TestMethod]
        public async Task File_ListAll()
        {
            var pbFile = Path.Combine(Path.GetTempPath(), "pb.xml");
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, MockFileSystem>()
                .AddSingleton(new FileSettings { Path = pbFile });
            var sp = services.BuildServiceProvider();

            var fs = sp.GetService<IFileSystem>();
            fs.Directory.CreateDirectory(Path.GetTempPath());
            fs.File.WriteAllText(pbFile,
@"<?xml version=""1.0""?>
<ArrayOfPhonebookEntry xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <PhonebookEntry>
    <Id>1</Id>
    <FirstName>First</FirstName>
    <LastName>Last</LastName>
    <BirthYear>1800</BirthYear>
  </PhonebookEntry>
</ArrayOfPhonebookEntry>");

            var sut = sp.GetService<IPhonebook>();

            var list = await sut.GetList();

            Assert.AreEqual(1, list.Count);

            var item1 = list[0];
            Assert.AreEqual(1, item1.Id);
            Assert.AreEqual("First", item1.FirstName);
            Assert.AreEqual("Last", item1.LastName);
            Assert.AreEqual(1800, item1.BirthYear);
        }


        [TestMethod]
        public async Task File_ListFilteredByFirstName()
        {
            var pbFile = Path.Combine(Path.GetTempPath(), "pb.xml");
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, MockFileSystem>()
                .AddSingleton(new FileSettings { Path = pbFile });
            var sp = services.BuildServiceProvider();

            var fs = sp.GetService<IFileSystem>();
            fs.Directory.CreateDirectory(Path.GetTempPath());
            fs.File.WriteAllText(pbFile, SamplePhonebook);

            var sut = sp.GetService<IPhonebook>();

            var list = await sut.GetList("first");

            Assert.AreEqual(1, list.Count);

            var item1 = list[0];
            Assert.AreEqual(1, item1.Id);
            Assert.AreEqual("First", item1.FirstName);
            Assert.AreEqual("Last", item1.LastName);
            Assert.AreEqual(1800, item1.BirthYear);
        }


        [TestMethod]
        public async Task File_ListFilteredByLastName()
        {
            var pbFile = Path.Combine(Path.GetTempPath(), "pb.xml");
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, MockFileSystem>()
                .AddSingleton(new FileSettings { Path = pbFile });
            var sp = services.BuildServiceProvider();

            var fs = sp.GetService<IFileSystem>();
            fs.Directory.CreateDirectory(Path.GetTempPath());
            fs.File.WriteAllText(pbFile, SamplePhonebook);

            var sut = sp.GetService<IPhonebook>();

            var list = await sut.GetList("last");

            Assert.AreEqual(2, list.Count);

            var item1 = list[0];
            Assert.AreEqual(1, item1.Id);
            Assert.AreEqual("First", item1.FirstName);
            Assert.AreEqual("Last", item1.LastName);
            Assert.AreEqual(1800, item1.BirthYear);
        }


        [TestMethod]
        public async Task File_ListFilteredByBirth()
        {
            var pbFile = Path.Combine(Path.GetTempPath(), "pb.xml");
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, MockFileSystem>()
                .AddSingleton(new FileSettings { Path = pbFile });
            var sp = services.BuildServiceProvider();

            var fs = sp.GetService<IFileSystem>();
            fs.Directory.CreateDirectory(Path.GetTempPath());
            fs.File.WriteAllText(pbFile, SamplePhonebook);

            var sut = sp.GetService<IPhonebook>();

            var list = await sut.GetList("19");

            Assert.AreEqual(1, list.Count);

            var item1 = list[0];
            Assert.AreEqual(2, item1.Id);
            Assert.AreEqual("Second", item1.FirstName);
            Assert.AreEqual("Last", item1.LastName);
            Assert.AreEqual(1900, item1.BirthYear);
        }


        [TestMethod]
        public async Task File_ListFilteredNoResult()
        {
            var pbFile = Path.Combine(Path.GetTempPath(), "pb.xml");
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, MockFileSystem>()
                .AddSingleton(new FileSettings { Path = pbFile });
            var sp = services.BuildServiceProvider();

            var fs = sp.GetService<IFileSystem>();
            fs.Directory.CreateDirectory(Path.GetTempPath());
            fs.File.WriteAllText(pbFile, SamplePhonebook);

            var sut = sp.GetService<IPhonebook>();

            var list = await sut.GetList("no-such-string");

            Assert.AreEqual(0, list.Count);
        }


        [TestMethod]
        public async Task File_AddDelete()
        {
            var pbFile = Path.Combine(Path.GetTempPath(), "pb.xml");
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, MockFileSystem>()
                .AddSingleton(new FileSettings { Path = pbFile });
            var sp = services.BuildServiceProvider();

            var fs = sp.GetService<IFileSystem>();
            fs.Directory.CreateDirectory(Path.GetTempPath());

            var sut = sp.GetService<IPhonebook>();

            await sut.Add(
                new PhonebookEntry { Id = 1, FirstName = "fname1", LastName = "lname1", BirthYear = 2000 });

            var list = await sut.GetList();
            Assert.AreEqual(1, list.Count);

            var item1 = list[0];
            Assert.AreEqual(1, item1.Id);
            Assert.AreEqual("fname1", item1.FirstName);
            Assert.AreEqual("lname1", item1.LastName);
            Assert.AreEqual(2000, item1.BirthYear);

            await sut.Delete(1);

            list = await sut.GetList();
            Assert.AreEqual(0, list.Count);
        }

        #endregion
    }
}
