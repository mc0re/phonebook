﻿using System;
using System.Data.SqlClient;
using System.IO.Abstractions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using PhonebookLib;


namespace WebPhonebook
{
    public class Startup
    {
        #region Configuration

        /// <summary>
        /// Configuration.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            var source = Environment.GetEnvironmentVariable("SOURCE");
            SetSource(services, source);

            services
                .AddSingleton<IFileSystem, FileSystem>()
                .AddSingleton(new FileSettings { Path = FileSettings.DefaultPath })
                .AddSingleton(new SqlSettings
                {
                    ConnectionType = typeof(SqlConnection),
                    ConnectionString = SqlSettings.DefaultConnection
                })
                .AddSingleton(new FirebaseSettings
                {
                    CredentialsPath = FirebaseSettings.DefaultCredentialsPath,
                    ProjectName = FirebaseSettings.DefaultProjectName
                });

            services
                .AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }


        /// <summary>
        /// Use configuration.
        /// </summary>
        public void Configure(IApplicationBuilder appl, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                appl.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //appl.UseHsts();
            }

            appl.UseStaticFiles()
                //.UseHttpsRedirection()
                .UseMvc();
        }

        #endregion


        #region Utility

        private static void SetSource(IServiceCollection services, string definition)
        {
            switch (definition)
            {
                case "sql":
                    services.AddSingleton<IPhonebook, SqlPhonebook>();
                    break;

                case "fb":
                    services.AddSingleton<IPhonebook, FirebasePhonebook>();
                    break;

                case "file":
                    services.AddSingleton<IPhonebook, FilePhonebook>();
                    break;

                default:
                    throw new ArgumentException($"Unknown source definition '{definition}'.");
            }
        }

        #endregion
    }
}
