﻿namespace WebPhonebook.Dto
{
    public class PhonebookEntryDto
    {
        #region Properties

        /// <summary>
        /// Entry ID.
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Person's first name.
        /// </summary>
        public string FirstName { get; set; }


        /// <summary>
        /// Person's last name.
        /// </summary>
        public string LastName { get; set; }


        /// <summary>
        /// Year of birth.
        /// </summary>
        public int BirthYear { get; set; }

        #endregion
    }
}
