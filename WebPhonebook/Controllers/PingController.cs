﻿using System;
using Microsoft.AspNetCore.Mvc;


namespace WebPhonebook.Controllers
{
    [Route("")]
    [ApiController]
    public class PingController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Ping()
        {
            return DateTime.Now.ToString();
        }
    }
}