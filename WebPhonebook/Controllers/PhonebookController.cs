﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhonebookLib;
using WebPhonebook.Dto;


namespace WebPhonebook.Controllers
{
    [Route("pb")]
    [ApiController]
    public class PhonebookController : ControllerBase
    {
        #region Fields

        private readonly IPhonebook mPhonebook;

        #endregion


        #region Init and clean-up

        public PhonebookController(IPhonebook phonebook)
        {
            mPhonebook = phonebook;
        }

        #endregion


        #region API 

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PhonebookEntryDto>>> Get()
        {
            var list = await mPhonebook.GetList();

            return Ok(
                from e in list
                select new PhonebookEntryDto
                {
                    Id = e.Id,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    BirthYear = e.BirthYear
                });
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<PhonebookEntryDto>> Get(int id)
        {
            var entry = (await mPhonebook.GetList(id.ToString())).
                Where(e => e.Id == id).FirstOrDefault();

            return Ok(new PhonebookEntryDto
            {
                Id = entry.Id,
                FirstName = entry.FirstName,
                LastName = entry.LastName,
                BirthYear = entry.BirthYear
            });
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] PhonebookEntryDto value)
        {
            var entry = new PhonebookEntry
            {
                FirstName = value.FirstName,
                LastName = value.LastName,
                BirthYear = value.BirthYear
            };

            await mPhonebook.Add(entry);

            return NoContent();
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await mPhonebook.Delete(id);

            return NoContent();
        }

        #endregion
    }
}
