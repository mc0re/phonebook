﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhonebookLib;
using WebPhonebook.Dto;


namespace WebPhonebook.Controllers
{
    [Route("ui")]
    public class UiController : Controller
    {
        #region Fields

        private readonly IPhonebook mPhonebook;

        #endregion


        #region Init and clean-up

        public UiController(IPhonebook phonebook)
        {
            mPhonebook = phonebook;
        }

        #endregion


        #region API

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var list =
                from e in await mPhonebook.GetList()
                select new PhonebookEntryDto
                {
                    Id = e.Id,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    BirthYear = e.BirthYear
                };

            return View("Index", list.ToList());
        }

        #endregion
    }
}
