﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO.Abstractions;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PhonebookLib;


namespace ConsoleApp
{
    public class Program
    {
        #region Main

        static async Task Main(string[] args)
        {
            Console.WriteLine("PhoneBook console application.");
            Console.WriteLine();

            // DependencyInjection with default settings
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, FileSystem>();

            var argIdx = 0;

            if (args.Length > 0 && args[argIdx] == "-s")
            {
                argIdx++;
                if (argIdx == args.Length)
                    throw new ArgumentException("-s shall be followed by a source definition.");

                SetSource(services, args[argIdx]);

                argIdx++;
            }

            // Settings
            services
                .AddSingleton(new FileSettings { Path = FileSettings.DefaultPath })
                .AddSingleton(new SqlSettings
                {
                    ConnectionType = typeof(SqlConnection),
                    ConnectionString = SqlSettings.DefaultConnection
                })
                .AddSingleton(new FirebaseSettings
                {
                    CredentialsPath = FirebaseSettings.DefaultCredentialsPath,
                    ProjectName = FirebaseSettings.DefaultProjectName
                });
            var provider = services.BuildServiceProvider();

            var cmd = argIdx == args.Length ? CommandLine.List : args[argIdx];
            argIdx++;

            switch (cmd)
            {
                case CommandLine.List:
                    ReadOnlyCollection<PhonebookEntry> list;
                    if (argIdx < args.Length)
                    {
                        list = await provider.GetRequiredService<IPhonebook>().GetList(args[argIdx]);
                    }
                    else
                    {
                        list = await provider.GetRequiredService<IPhonebook>().GetList();
                    }
                    PrintEntries(list);
                    break;

                case CommandLine.Add:
                    var entry = RetrieveEntryData();
                    await provider.GetRequiredService<IPhonebook>().Add(entry);
                    break;

                case CommandLine.Delete:
                    var id = RetrieveEntryId();
                    await provider.GetRequiredService<IPhonebook>().Delete(id);
                    break;

                case CommandLine.Help:
                    Console.WriteLine(CommandLine.HelpText);
                    break;

                default:
                    Console.WriteLine(CommandLine.TryHelpText);
                    break;
            }
        }

        #endregion


        #region Utility

        private static void SetSource(IServiceCollection services, string definition)
        {
            switch (definition)
            {
                case "sql":
                    services.AddSingleton<IPhonebook, SqlPhonebook>();
                    break;

                case "fb":
                    services.AddSingleton<IPhonebook, FirebasePhonebook>();
                    break;

                case "file":
                    // Already File
                    break;

                default:
                    throw new ArgumentException($"Unknown source definition '{definition}'.");
            }
        }


        /// <summary>
        /// Print all entries to the console.
        /// </summary>
        private static void PrintEntries(IEnumerable<PhonebookEntry> list)
        {
            Console.WriteLine("{0,4} {1,-30} {2}", "Id", "Name", "Birth year");
            Console.WriteLine(string.Join("", Enumerable.Repeat("-", 80)));

            foreach (var entry in list)
            {
                Console.WriteLine("{0,4} {1,-30} {2}",
                    entry.Id, entry.FirstName + " " + entry.LastName, entry.BirthYear);
            }
        }


        /// <summary>
        /// Get phonebook entry from the user.
        /// </summary>
        private static PhonebookEntry RetrieveEntryData()
        {
            var entry = new PhonebookEntry();

            Console.Write("First name: ");
            entry.FirstName = Console.ReadLine();

            Console.Write("Last name: ");
            entry.LastName = Console.ReadLine();

            Console.Write("Birth year: ");
            var year = Console.ReadLine();
            entry.BirthYear = int.Parse(year);

            return entry;
        }


        /// <summary>
        /// Get a phonebook entry ID.
        /// </summary>
        private static int RetrieveEntryId()
        {
            Console.Write("ID to delete: ");
            var id = Console.ReadLine();
            return int.Parse(id);
        }

        #endregion
    }
}
