# Coding Pirates professional programming repository

## Code descripton

This is a set of primitive .NET applications.
They all are managing a trivial phonebook using the same library. The applications are:
- Command-line console applicatin
- WPF application
- Web application

### Common library

The library establishes a few possible sources for the data. Their definition is the same in all projects.
- An XML file (`file`)
- An SQL database (`sql`)
- A FireBase database (`fb`)

The library allows listing the records, adding and removing them.

### Console application

This is a command-line interface to the mentioned library.

The source is defined by a `-s` option. Then the command follows - `list`, `add`, or `del`.

### WPF application

This is a native Windows GUI over the phonebook library.

The source is defined by a `-s` option on the command line.

### Web application

This is a .NET Core service and an HTML page providing the Web UI to the ubiquitous phonebook library.

The source is defined by a `SOURCE` environment variable.


## Installation and execution

1. Install *Visual Studio Community Edition* with *.NET Core 2.2*, *.NET Framework*, some *SQL* tools, and *Razor*. Use *C#* keyboard.
    * **Advanced**: installer is created by this command
```
./vs_community.exe --layout C:\VS2019 `
	--add Microsoft.VisualStudio.Workload.CoreEditor `
	--add Microsoft.VisualStudio.Workload.ManagedDesktop `
	--add Microsoft.Net.Core.Component.SDK.2.2 `
	--add Microsoft.NetCore.ComponentGroup.DevelopmentTools.2.2 `
	--add Microsoft.NetCore.ComponentGroup.Web.2.2 `
	--add Microsoft.VisualStudio.Component.EntityFramework `
	--add Microsoft.VisualStudio.Component.Git `
	--add Microsoft.VisualStudio.Component.SQL.LocalDB.Runtime `
	--add Microsoft.VisualStudio.Component.SQL.SSDT `
	--add Microsoft.VisualStudio.Component.MSODBC.SQL `
	--lang en-US
```
2. Clone the repository via its *HTTPS* link
    * **Advanced**:
        1) Create an account on GitLab
        2) Fork the project to your account
        3) Create SSH keys for the account (see https://gitlab.com/profile/keys)
        4) Clone your repository using *SSH* link
3. Open the three solutions in *Visual Studio*

### File source

Open project propeties, make sure the source is `file` and command is `add`.

The storage file will be created in `C:\Temp`.
Open this file `C:\Temp\pb.xml` in *Visual Studio*, check its contents.

Try running the project from the command line (`dotnet .\ConsolePhonebook\bin\Debug\netcoreapp2.2\ConsolePhonebook.dll -s ...`).

### SQL source

Open *SQL Server Object Explorer*, in *LocalDb* add a new database `Phonebook`.

Change the source to `sql`, command `add`.

Open the newly created table `Persons` (*View Data* menu item).

**Advanced**: install SQL Server Management Studio (https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017)

### Firebase source

Create an account on https://firebase.google.com/ using your Google email. Create a new project.

Create a JSON key (*Console* -> *Service accounts* -> *...* -> *Create Key*), download the credentials file, and set its path in `FirebaseSettings.cs`. Set the project name there as well.

Change the source to `fb`, command `add`.

Open the *Firebase* web interface, check the data.

### WPF application

Set the source, e.g. `-s sql`.

Use filter box for filtering, `+` button to add a new filled entry, `x` to delete an existing one.

### Web application

Switch to *Kestrel file*, *Kestrel SQL* or *Kestrel Firebase*. Upon the first start, trust the SSL certificate.

### Deployment to GCloud

Make sure you have a Cloud Build and Cloud Run services for your Firebase project (see https://console.cloud.google.com).

The commands are given in `deploy.bat`, replace the project name *phonebook-6f371* with yours.
```
gcloud builds submit --tag gcr.io/phonebook-6f371/phonebook
gcloud beta run deploy phonebook --image gcr.io/phonebook-6f371/phonebook
```