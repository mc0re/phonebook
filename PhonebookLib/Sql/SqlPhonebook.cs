﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace PhonebookLib
{
    public class SqlPhonebook : IPhonebook
    {
        #region Fields

        private DbConnection mConnection;

        #endregion


        #region Init and clean-up

        public SqlPhonebook(SqlSettings settings)
        {
            mConnection = (DbConnection)Activator.CreateInstance(
                settings.ConnectionType, settings.ConnectionString);
            EnsureTable(mConnection);
        }

        #endregion


        #region IPhonebook implementation

        async Task IPhonebook.Add(PhonebookEntry entry)
        {
            var cmd = mConnection.CreateCommand();
            cmd.CommandText = @"INSERT INTO Persons (FirstName, LastName, BirthYear) VALUES (@FN, @LN, @BY)";
            cmd.Parameters.Add(new SqlParameter("@FN", entry.FirstName));
            cmd.Parameters.Add(new SqlParameter("@LN", entry.LastName));
            cmd.Parameters.Add(new SqlParameter("@BY", entry.BirthYear));

            mConnection.Open();
            await cmd.ExecuteNonQueryAsync();
            mConnection.Close();
        }


        async Task IPhonebook.Delete(int id)
        {
            var cmd = mConnection.CreateCommand();
            cmd.CommandText = @"DELETE FROM Persons WHERE Id = @ID";
            cmd.Parameters.Add(new SqlParameter("@ID", id));

            mConnection.Open();
            await cmd.ExecuteNonQueryAsync();
            mConnection.Close();
        }


        async Task<ReadOnlyCollection<PhonebookEntry>> IPhonebook.GetList(string filter)
        {
            var list = new List<PhonebookEntry>();

            var cmd = mConnection.CreateCommand();
            cmd.CommandText = "SELECT * FROM Persons";

            if (filter != null)
            {
                cmd.CommandText += $" WHERE FirstName LIKE '%{filter}%' OR LastName LIKE '%{filter}%'" +
                                   $" OR CAST(BirthYear AS VARCHAR(4)) LIKE '%{filter}%'";
            }

            mConnection.Open();

            using (var rdr = await cmd.ExecuteReaderAsync())
            {
                while (rdr.Read())
                {
                    var entry = new PhonebookEntry();
                    entry.Id = rdr.GetInt32(0);
                    entry.FirstName = rdr.GetString(1);
                    entry.LastName = rdr.GetString(2);
                    entry.BirthYear = rdr.GetInt32(3);

                    list.Add(entry);
                }
            }

            mConnection.Close();

            return list.AsReadOnly();
        }

        #endregion


        #region Utility

        private static void EnsureTable(DbConnection conn)
        {
            var cmd = conn.CreateCommand();
            cmd.CommandText = @"
IF NOT EXISTS (SELECT * 
               FROM INFORMATION_SCHEMA.TABLES 
               WHERE TABLE_NAME = 'Persons')
BEGIN
    CREATE TABLE Persons (
        Id INT IDENTITY(1,1) PRIMARY KEY,
        FirstName VARCHAR(1000) NOT NULL,
        LastName VARCHAR(1000) NOT NULL,
        BirthYear INT NOT NULL
    );
END";
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        #endregion
    }
}
