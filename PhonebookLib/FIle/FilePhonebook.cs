﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;


namespace PhonebookLib
{
    public class FilePhonebook : IPhonebook
    {
        #region Fields

        private readonly string mPhonebookPath;

        private readonly IFileSystem mFs;

        private readonly XmlSerializer mSerializer = new XmlSerializer(typeof(List<PhonebookEntry>));

        #endregion


        #region Init and clean-up

        public FilePhonebook(IFileSystem fs, FileSettings sett)
        {
            mFs = fs;
            mPhonebookPath = sett.Path;
        }

        #endregion


        #region IPhonebook API

        /// <inheritdoc/>
        Task IPhonebook.Add(PhonebookEntry entry)
        {
            var list = GetAllInternal();

            entry.Id = list.Any() ? list.Max(e => e.Id) + 1 : 1;
            list.Add(entry);

            WriteAllInternal(list);

            return Task.CompletedTask;
        }


        /// <inheritdoc/>
        Task IPhonebook.Delete(int id)
        {
            var list = GetAllInternal();

            list.RemoveAll(e => e.Id == id);

            WriteAllInternal(list);

            return Task.CompletedTask;
        }


        /// <inheritdoc/>
        Task<ReadOnlyCollection<PhonebookEntry>> IPhonebook.GetList(string filter)
        {
            var list = GetAllInternal().Where(e => e.IsMatch(filter)).ToList();

            return Task.FromResult(list.AsReadOnly());
        }

        #endregion


        #region Utility

        private List<PhonebookEntry> GetAllInternal()
        {
            List<PhonebookEntry> list;

            if (mFs.File.Exists(mPhonebookPath))
            {
                using (var f = mFs.File.OpenRead(mPhonebookPath))
                {
                    list = (List<PhonebookEntry>)mSerializer.Deserialize(f);
                }
            }
            else
            {
                list = new List<PhonebookEntry>();
                WriteAllInternal(list);
            }

            return list;
        }


        private void WriteAllInternal(IEnumerable<PhonebookEntry> list)
        {
            mFs.Directory.CreateDirectory(Path.GetDirectoryName(mPhonebookPath));

            using (var f = mFs.File.Create(mPhonebookPath))
            {
                mSerializer.Serialize(f, list);
            }
        }

        #endregion
    }
}
