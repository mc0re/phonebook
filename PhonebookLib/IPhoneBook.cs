﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace PhonebookLib
{
    /// <summary>
    /// Generalized functionality for storing and retrieving phonebook entries.
    /// </summary>
    public interface IPhonebook
    {
        /// <summary>
        /// Get all entries as a list.
        /// </summary>
        Task<ReadOnlyCollection<PhonebookEntry>> GetList(string filter = "");


        /// <summary>
        /// Add a new entry/
        /// </summary>
        Task Add(PhonebookEntry entry);


        /// <summary>
        /// Delete an existing entry by its ID.
        /// </summary>
        Task Delete(int id);
    }
}